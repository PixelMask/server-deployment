import hashlib
import FileUtils


class FileHashManager:

    def __init__(self, hash_file_path):
        self.hash_file_path = hash_file_path
        self.hash_json = FileUtils.read_json_from_file(hash_file_path)

    # Checks if the hash value of the contents of the file at the given path matches the hash value saved for that path
    # in the hash json.
    def file_updated(self, file_path):
        return file_path not in self.hash_json or self.file_hash(file_path) != self.hash_json[file_path]

    # Writes the hash value of the given file's contents to the file hash json.
    def update_file_hash(self, file_path):
        self.hash_json[file_path] = self.file_hash(file_path)
        FileUtils.write_json_to_file(self.hash_file_path, self.hash_json)

    # Generates a hash value from the contents of the file at the given path.
    @staticmethod
    def file_hash(file_path):
        with open(FileUtils.resolve_path(file_path), 'rb') as f:
            return hashlib.md5(f.read()).hexdigest()

