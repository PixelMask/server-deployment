import sys
import os
import FileUtils
import DeploymentActor
from FileHashManager import FileHashManager
from DeploymentConnector import DeploymentConnector


# Parses the directory of the config file from the config path passed as the script's first argument.
def get_config_path():
    if len(sys.argv) < 2 or FileUtils.path_delimiter not in sys.argv[1]:
        path = os.getcwd()
    else:
        path = os.path.dirname(sys.argv[1])
    return os.path.abspath(os.path.realpath(path))


# Parses the name of the config file from the config path passed as the script's first argument.
def get_config_name():
    if len(sys.argv) < 2:
        return 'deploy-config.json'
    if FileUtils.path_delimiter not in sys.argv[1]:
        return sys.argv[1]
    return sys.argv[1].rsplit(FileUtils.path_delimiter, 1)[1]


print("\nDeployment started...\n")

# Read the config values from the file defined in the first python argument. Defaults to 'deploy-config.json' if
# none provided. Updates the working directory to the config file's directory so config file paths are relative to its
# location.
config_directory = get_config_path()
os.chdir(config_directory)
config_file = config_directory + FileUtils.path_delimiter + get_config_name()
print("Using config JSON at '" + config_file + "'\n")
config = FileUtils.read_json_from_file(config_file)

# Read file hash file path from config.
hash_file = 'file_hash.json' if 'hash_file_path' not in config else FileUtils.resolve_path(config['hash_file_path'])
print("Using file hash at '" + hash_file + "'\n")

# Read SSH key from config.
key_path = "" if 'key_file_path' not in config else FileUtils.resolve_path(config['key_file_path'])

password = "" if 'password' not in config else config['password']

# Open a new connection.
connection = DeploymentConnector(config['user'], config['host'], FileHashManager(hash_file), password, key_path)

# Execute the config's actions.
DeploymentActor.execute(config['actions'], connection)

# Close the server connections.
connection.close()

print("Deployment successful")
