import io
import os
import json
from sys import platform

win32_path_delimiter = "\\"
unix_path_delimiter = "/"

if platform == "win32":
    path_delimiter = win32_path_delimiter
else:
    path_delimiter = unix_path_delimiter


def resolve_path(path):
    path = validate_path_delimiters(path)
    if path.startswith(path_delimiter):
        return path
    return os.path.abspath(os.path.realpath(path))


def resolve_file_name(path):
    return path if path_delimiter not in path else path.rsplit(path_delimiter, 1)[1]


def validate_path_delimiters(path):
    # Updates path to have the correct OS specific delimiters
    win32_delimiter = win32_path_delimiter in path
    unix_delimiter = unix_path_delimiter in path
    if win32_delimiter or unix_delimiter:
        if win32_delimiter and path_delimiter not in path:
            return path.replace(win32_path_delimiter, unix_path_delimiter)
        elif unix_delimiter and path_delimiter not in path:
            return path.replace(unix_path_delimiter, win32_path_delimiter)
    return path


# Parses out the given path into a list of its folder names.
def split_path(path):
    all_parts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:
            all_parts.insert(0, parts[0])
            break
        elif parts[1] == path:
            all_parts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            all_parts.insert(0, parts[1])
    return all_parts


def write_json_to_file(file_path, data):
    json_file = open_json_file(file_path, True)
    print("Writing JSON to '" + file_path + "'\n")
    json.dump(data, json_file)


def read_json_from_file(file_path):
    return json.load(open_json_file(file_path, True))


# Resolves the given file path and tries to read it as a json file.
def open_json_file(file_path, create_if_missing=False):
    file_path = resolve_path(file_path)
    try:
        return open(file_path, 'r+')
    except IOError:
        if not create_if_missing:
            print("Failed to read json file from '" + file_path + "'\n")
            return
        print("No json file found at '" + file_path + "'. Creating...\n")
        json_file = io.open(file_path, 'w+', encoding='utf-8')
        json_file.write("{}")
        json_file.flush()
        json_file.seek(0)
        return json_file
