# Server Deployment

This tool is used for batching SSH and SFTP commands together for quick server deployments and updates. Upload actions
and SSH commands can be easily mixed and matched in the config file described below. Previously uploaded files are 
checked for modifications before reuploading to reduce deployment time and bandwidth usage.

## Installation

Install Python 3.8.1 or higher and then use the package manager [pip](https://pip.pypa.io/en/stable/) to install the 
dependencies by executing the command shown below in the source directory.

```bash
pip install -r requirements.txt
```
## Usage

The script reads the server information and the actions to execute from a JSON config file. The path to the config file 
can be provided as the first argument when executing the script or if no path is provided the script will default to the 
`deploy-config.json` file in the source directory. The config argument path can be relative, absolute, or just the file name if 
it's in the same directory.
```bash
python deploy.py ../my/current/project/deploy-config.json
```
Properties for the config file are detailed below.

* `user`: The server's SSH username. (Required)
* `host`: The server's SSH host. (Required)
* `key_file_path` or `password`: The server's authentication defined as either a password or a path to the server's 
  cert file. The path can be relative, absolute, or just the file name if it's in the same directory. `key_file_path` 
  will take precidence if both fields are defined. (Required)
* `hash_file_path`: The path of the file that keeps track of what files have been modified since last deploy. This is
used to limit uploads to only files that have been updated. If no value is provided the script will automatically create 
a `file_hash.json` file in the source directory.
* `actions`: Contains an array of actions to be performed. There are two types of action.
    1. "upload" which copies local files to the specified server directory.
        * The file path(s) can be relative, absolute, or just the file name if it's in the same directory. If the path 
        ends in a '/' the entire directory will be uploaded.
    2. "command" which executes the given commands on the server.

Examples of both actions can be found below and in the default `deploy-config.json` file.

```
"actions" : [
        {
            "type": "upload",
            "files" : [
                "../path/to/localFile1.txt",
                "/path/to/localFile2.py",
                "localFile3.js",
                "/path/to/directory/to/upload/"
            ],
            "remote": "/path/to/remote/directory"
        },
        {
            "type": "command",
            "entries": "sudo systemctl restart myService.service"
        },
        {
            "type": "upload",
            "files" : "../path/to/local/file1.txt",
            "remote": "/path/to/remote/directory2"
        },
        {
            "type": "command",
            "entries": [
                "cd /lets/go/to/this/directory",
                "python theBestScript.py"
            ]
        }
    ]
```

**Note:** The upload actions will create remote directories that don't already exist and `sudo chown -R {USER}` is 
silently executed on every remote directory uploaded to.

**Note:** The `~` path prefix does not work with the upload action type's `remote` property.

## License
[MIT](https://choosealicense.com/licenses/mit/)
