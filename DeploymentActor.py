import os
import FileUtils


# Iterate through and execute the actions
def execute(actions, connector):
    for action in actions:
        action_type = action['type']
        if action_type == "upload":
            do_uploads(action['files'], action['remote'], connector)
        elif action_type == "command":
            do_commands(action['entries'], connector)
        else:
            print("'" + action_type + "' is not a valid action type.\n")


# Iterate through and execute the file uploads.
def do_uploads(files, remote_path, connector):
    if type(files) is list:
        for file_path in files:
            do_upload(file_path, remote_path, connector)
    else:
        do_upload(files, remote_path, connector)


# Executes either a file upload or directory upload depending on if the given path has a '.' or ends with a '/'.
def do_upload(local_path, remote_path, connector):
    path = FileUtils.resolve_path(local_path)
    if os.path.isdir(path):
        connector.upload_directory(local_path, remote_path)
    else:
        connector.upload_file(local_path, remote_path)


# Iterate through and execute the commands.
def do_commands(entries, connector):
    if type(entries) is list:
        for entry in entries:
            connector.command(entry)
    else:
        connector.command(entries)
