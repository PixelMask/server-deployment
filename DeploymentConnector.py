import os
import paramiko
import warnings
import FileUtils

warnings.filterwarnings(action='ignore', module='.*paramiko.*')


class DeploymentConnector:

    def __init__(self, user, host, file_hash_manager, password="", key_file_path=""):
        self.user = user
        self.file_hash_manager = file_hash_manager
        self.ssh_client = paramiko.SSHClient()
        self.ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print("Connecting...\n")
        if key_file_path:
            self.ssh_client.connect(hostname=host,
                                    username=user,
                                    pkey=paramiko.RSAKey.from_private_key_file(key_file_path))
        elif password:
            self.ssh_client.connect(hostname=host,
                                    username=user,
                                    password=password)
        else:
            print("Must define either a password or ssh key file path\nFailed to connect\n")
            return
        self.sftp_client = self.ssh_client.open_sftp()
        self.sftp_client.sshclient = self.ssh_client
        print("Connected\n")

    # Navigates the sftp client to the given folder within the current directory. Creates it if it doesn't exist.
    def create_chdir_folder(self, folder):
        try:
            self.sftp_client.chdir(folder)
        except IOError:
            self.sftp_client.mkdir(folder)
            self.sftp_client.chdir(folder)

    # Navigate the sftp client to the given directory. Creating any folders that don't exist.
    def create_chdir_path(self, directory):
        self.sftp_client.chdir("/")
        path_parts = FileUtils.split_path(directory)
        for i in range(len(path_parts)):
            if path_parts[i] != '/':
                self.create_chdir_folder(path_parts[i])

    # Upload the specified file to the given remote directory.
    def upload_file(self, file_path, remote_directory):
        file_path = FileUtils.resolve_path(file_path)
        if not self.file_hash_manager.file_updated(file_path):
            return
        file_name = FileUtils.resolve_file_name(file_path)
        print("Uploading: '" + file_path + "' -> '"+ remote_directory + "/" + file_name + "'\n")
        self.command("sudo chown -R " + self.user + " " + remote_directory, False)
        self.create_chdir_path(remote_directory)
        self.sftp_client.put(file_path, remote_directory + "/" + file_name)
        self.file_hash_manager.update_file_hash(file_path)

    # Upload all files in the given directory to the given remote directory.
    def upload_directory(self, directory_path, remote_directory):
        for file_name in os.listdir(directory_path):
            path = directory_path + FileUtils.path_delimiter + file_name
            if os.path.isfile(path):
                self.upload_file(path, remote_directory)
            else:
                self.upload_directory(path, remote_directory + "/" + file_name)

    # Execute the given command on the server.
    def command(self, command, log=True):
        stdin, stdout, stderr = self.ssh_client.exec_command(command)
        response = stdout.read()
        error = stderr.read()
        if log:
            print("Command: '" + command + "'\n")
            if response:
                print("Command Response: '" + response.decode("utf-8").replace("\n", "") + "'\n")
            if error:
                print("Command Error: '" + error.decode("utf-8").replace("\n", "") + "'\n")

    # Close the connections.
    def close(self):
        self.sftp_client.close()
        self.ssh_client.close()
        print("Connection closed\n")

